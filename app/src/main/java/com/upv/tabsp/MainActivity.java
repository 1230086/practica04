package com.upv.tabsp;





import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

    public final static String EXTRA_MESSAGE = "com.upv.tabs.MESSAGE";
    private Button button;
    private Button btnG;
    private Button btnL;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Resources res = getResources();
        TabHost tabHost = (TabHost) findViewById(R.id.tabhost);
        tabHost.setup();

        TabSpec spec1 = tabHost.newTabSpec("Tab1");
        spec1.setContent(R.id.tab1);
        spec1.setIndicator("Área");

        TabSpec spec2 = tabHost.newTabSpec("Tab2");
        spec2.setContent(R.id.tab2);
        spec2.setIndicator("Distancia");

        TabSpec spec3 = tabHost.newTabSpec("Tab3");
        spec3.setContent(R.id.tab3);
        spec3.setIndicator("Grados");

        tabHost.addTab(spec1);
        tabHost.addTab(spec2);
        tabHost.addTab(spec3);

        addListenerOnButton();

    }



    private void addListenerOnButton() {
        // TODO Auto-generated method stub
        button  = (Button) findViewById(R.id.calculo);
        btnL = (Button) findViewById(R.id.calculoL);
        btnG = (Button) findViewById(R.id.calculoG);




        button.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                //Intent intent = new Intent(MainActivity.this, DisplayMessageActivity.class);
                EditText editText = (EditText) findViewById(R.id.lado);
                TextView resu = (TextView) findViewById(R.id.resultado);
                Double lado = Double.valueOf(editText.getText().toString());
                Double area = lado*lado;
                String resul = String.valueOf(area);
                //intent.putExtra("resultado", resul);
                //startActivity(intent);
                resu.setText("El área es: "+resul);
            //}
            }
        });

        btnL.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                //Intent intent = new Intent(MainActivity.this, DisplayMessageActivity.class);
                EditText editText = (EditText) findViewById(R.id.medida);
                TextView resu = (TextView) findViewById(R.id.resultadoL);

                Double milla = Double.valueOf(editText.getText().toString());
                Double res = milla * 1.609;
                String resul = String.valueOf(res);
                //intent.putExtra("resultado", resul);
                //startActivity(intent);
                resu.setText("Las millas son: "+resul);
                //}
            }
        });

        btnG.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                //Intent intent = new Intent(MainActivity.this, DisplayMessageActivity.class);
                EditText editText = (EditText) findViewById(R.id.grado);
                TextView resu = (TextView) findViewById(R.id.resultadoG);

                Double grado = Double.valueOf(editText.getText().toString());
                Double res = grado * 1.8 + 32;
                String resul = String.valueOf(res);
                //intent.putExtra("resultado", resul);
                //startActivity(intent);
                resu.setText("Los grados F° son: "+resul);
                //}
            }
        });
    }




}